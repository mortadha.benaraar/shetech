using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
   public int health = 100,currentHealth;
    [SerializeField] float lookRadius = 10f;
    [SerializeField] Player Player;
    // Start is called before the first frame update
    void Start()
    {
        Player = FindObjectOfType<Player>();
        currentHealth = health;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
    public void Attack(GameObject player)
    {
        if (Vector3.Distance(player.transform.position,this.transform.position) < 1f)
        {

            Debug.LogWarning("Attack State");
            Player.currentHealth -= 1;
            if (Player.currentHealth <= 0)
            {
                Debug.Log("Player Dead");
            }
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(Player.transform.position, transform.position);
        if ((distance <= lookRadius))
            {
            Attack(Player.gameObject);
        }
    }
}
