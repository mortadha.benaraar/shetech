using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
[System.Serializable]
public struct Moves
{
    public Tilemap tileMap;
}
public class Player : MonoBehaviour
{
    NavMeshAgent MyAgent;
    [SerializeField]
    LayerMask Clickable;
    [SerializeField]
    int column=25, row=25;
    Vector2 Hexa;
    [SerializeField] float lookRadius = 10f;
    [SerializeField]
    public int health=100,currentHealth;
    [SerializeField]
    Moves[,] moves;
    [SerializeField] Hexagon hex;
    [SerializeField] Transform enemy;
    bool SceneLoaded=false;
    private void Start()
    {
        Hexa = new Vector2(column, row);
        moves = new Moves[(int)Hexa.x, (int)Hexa.y];
        MyAgent = GetComponent<NavMeshAgent>();
        currentHealth = health;
       
    
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    private void Update()
    {
        if(enemy == null)
        {
            GetClosestEnemy(enemy);
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray myRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(myRay, out hitInfo, 100, Clickable))
            {
                float distance = Vector3.Distance(hitInfo.point, transform.position);
                if ((distance <= lookRadius) && (hex.centerPoint != hitInfo.point))
                    {
                    MyAgent.SetDestination(hitInfo.point);
                   
                } 
            }
            
        }
        Attack(enemy.gameObject);
    }
    private void OnCollisionStay(Collision collision)
    {
        if(CompareTag("Hex"))
        {
            this.transform.SetParent(collision.transform);
            Debug.LogWarning("Collision");
         
        }
    }
    public Transform GetClosestEnemy(Transform[] enemies)
    {
        Transform tMin = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPos = transform.position;
        foreach (Transform t in enemies)
        {
            float dist = Vector3.Distance(t.position, currentPos);
            if (dist < minDist)
            {
                tMin = t;
                minDist = dist;
            }
        }
        return tMin;
    }


    public void Attack(GameObject Enemies)
    {
        if(Vector3.Distance(this.transform.position,Enemies.transform.position)<1f)
        {
           
            Debug.Log("Attack State");
            if (SceneLoaded == false)
            {
                SceneManager.LoadScene("New Scene", LoadSceneMode.Additive);
                SceneLoaded = true;
            }
            enemy.currentHealth -= 1;
            if (enemy.currentHealth <= 0)
            {
                // Destroy(enemy.gameObject);
                Debug.Log("Enemy dead");
              

                SceneManager.UnloadSceneAsync("New Scene");
            }
         
        }
    }
}
