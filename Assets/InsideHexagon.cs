using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Hexagon
{
    public float halfSize;
    public Vector3 centerPoint;
    public Vector3 upperRightCorner;
    public Vector3 upperLeftCorner;
    public Vector3 upperCorner;
    public Vector3 lowerRightCorner;
    public Vector3 lowerLeftCorner;
    public Vector3 lowerCorner;

    public Hexagon(Vector3 centerPoint,float halfSize)
    {
        this.centerPoint = centerPoint;
        this.halfSize = halfSize;

        upperCorner = centerPoint + new Vector3(0, 0, +1) * halfSize *.5f;
        lowerCorner = centerPoint + new Vector3(0, 0, -1) * halfSize * .5f;
        upperRightCorner = centerPoint + new Vector3(1, 0, 0) * halfSize + new Vector3(0, 0, 1) * halfSize * .5f;
        upperLeftCorner = centerPoint + new Vector3(1, 0, 0) * halfSize + new Vector3(0, 0, 1) * halfSize * .5f;
        lowerRightCorner = centerPoint + new Vector3(1, 0, 0) * halfSize + new Vector3(0, 0, 1) * halfSize * .5f;
        lowerRightCorner = centerPoint + new Vector3(1, 0, 0) * halfSize + new Vector3(0, 0, 1) * halfSize * .5f;
    }
}
public class InsideHexagon : MonoBehaviour
{
   [SerializeField] Hexagon hexagon;
    // Start is called before the first frame update
    void Start()
    {
         hexagon = new Hexagon(new Vector3(0, 0, 0), .5f);
      //  Debug.Log(JsonUtility.ToJson(hexagon));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
